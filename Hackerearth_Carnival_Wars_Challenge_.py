#!/usr/bin/env python
# coding: utf-8

# # Hackerearth Carnival Wars Challenge 
# #By- Aarush Kumar
# #Dated: July 11,2021

# In[34]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import datetime as dt
get_ipython().run_line_magic('matplotlib', 'inline')
sns.set(color_codes=True)


# In[35]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Hackerearth Carnival Wars/train.csv")
test = pd.read_csv("/home/aarush100616/Downloads/Projects/Hackerearth Carnival Wars/test.csv")
sample = pd.read_csv("/home/aarush100616/Downloads/Projects/Hackerearth Carnival Wars/sample_submission.csv")


# In[36]:


df.head()


# ## EDA

# In[37]:


df.info()


# In[38]:


df.columns


# In[40]:


df.describe().T


# In[41]:


df.drop(['Product_id','Customer_name'],axis=1,inplace=True)


# In[42]:


df.head()


# In[43]:


df.shape


# In[44]:


df.isnull().sum()


# In[45]:


df.dropna(axis=0,inplace=True)


# In[46]:


df.isnull().sum()


# In[47]:


df.shape


# ## Removing Duplicate Rows

# In[48]:


df.drop_duplicates(inplace=True)
df.shape


# In[49]:


corr_matrix = df.corr()
corr_matrix['Selling_Price'].sort_values(ascending = False)


# In[50]:


plt.figure(figsize=(12,7))
sns.heatmap(corr_matrix,annot=True)


# In[51]:


def correlation(dataset, threshold):
    col_corr = set()  # Set of all the names of correlated columns
    corr_matrix = dataset.corr()
    for i in range(len(corr_matrix.columns)):
        for j in range(i):
            if abs(corr_matrix.iloc[i, j]) > threshold: # we are interested in absolute coeff value
                colname = corr_matrix.columns[i]  # getting the name of column
                col_corr.add(colname)
    return col_corr
correlation(df,0.8)


# In[52]:


for i in df.columns:
    print("Unique Values in Column {} are {}".format(i,len(df[i].unique())))


# In[53]:


from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import ExtraTreesRegressor


# In[54]:


X_temp = df.drop(['instock_date','Selling_Price'],axis=1)
Y_temp = df['Selling_Price']
lb = LabelEncoder()
X_temp['Loyalty_customer'] = lb.fit_transform(X_temp['Loyalty_customer']) 
X_temp['Product_Category'] = lb.fit_transform(X_temp['Product_Category'])


# In[55]:


model = ExtraTreesRegressor()
model.fit(X_temp,Y_temp)


# In[56]:


# plot graph of feature importance for better visualization
feat_import = pd.Series(model.feature_importances_,index = X_temp.columns)
feat_import.nlargest(13).plot(kind='bar')
plt.show()


# ## Data Visualization

# In[57]:


px.pie(df,names='Grade')


# ## Relation Between Maximum Price and Selling Price

# In[58]:


plt.figure(figsize=(12,7))
px.scatter(data_frame=df,x='Maximum_price',y='Selling_Price')


# ## Relation Between Minimum Price and Selling Price

# In[59]:


px.scatter(data_frame=df,x='Minimum_price',y='Selling_Price')


# ## Visualizing Outliers using Box Plot

# In[60]:


px.box(df,y=['Selling_Price','Maximum_price','Minimum_price'])


# In[61]:


px.histogram(data_frame=df,x='Market_Category',color='Grade')


# ### Loyal Customers in Different Product Category

# In[62]:


px.histogram(data_frame=df,x='Product_Category',color='Loyalty_customer')


# In[63]:


import plotly.figure_factory as ff


# In[64]:


sns.distplot(df.Maximum_price)


# In[65]:


sns.distplot(df.Minimum_price)


# In[66]:


sns.distplot(df.charges_1)


# In[67]:


x = df.Maximum_price
y = df.Minimum_price
z = df.charges_1
hist_data = [x,y,z]
group_labels = ['Maximum_price','Minimum_price','charges_1'] # name of the dataset
fig = ff.create_distplot(hist_data, group_labels,show_hist=False)
fig.show()


# In[68]:


sns.distplot(df['Selling_Price'])


# In[69]:


sns.kdeplot(df['Selling_Price'])


# In[70]:


(df[df['Selling_Price']<0]).shape


# In[71]:


df.shape


# In[72]:


index_name =df[df['Selling_Price']<0].index
df.drop(index_name,inplace=True) 
df.shape


# In[73]:


sns.kdeplot(df['Selling_Price'])


# In[74]:


## Normalising the Target Column


# In[75]:


df['Selling_Price'] = np.log1p(df.Selling_Price)
sns.kdeplot(df['Selling_Price'])


# In[76]:


sns.distplot(df['Selling_Price'])


# In[77]:


sns.pairplot(df)


# ## Building a Model

# In[78]:


df.instock_date = pd.to_datetime(df['instock_date'])


# In[79]:


df['instock_date_year'] = df['instock_date'].dt.year
df['instock_date_month'] = df['instock_date'].dt.month
df['instock_date_week'] = df['instock_date'].dt.week
df['instock_date_day'] = df['instock_date'].dt.day
df['instock_date_hour'] = df['instock_date'].dt.hour
df['instock_date_minute'] = df['instock_date'].dt.minute
df['instock_date_dayofweek'] = df['instock_date'].dt.dayofweek


# In[80]:


df.drop('instock_date',axis=1,inplace=True)


# In[81]:


df.drop('Stall_no',axis=1,inplace=True)


# In[82]:


df = pd.get_dummies(df,columns=['Loyalty_customer','Product_Category','Grade','Discount_avail'],drop_first=True)


# In[83]:


df.shape


# In[84]:


df.head()


# In[85]:


X = df.drop('Selling_Price',axis=1)
Y = df['Selling_Price']


# In[86]:


from sklearn.model_selection import train_test_split
X_train,X_test,Y_train,Y_test = train_test_split(np.array(X),np.array(Y),test_size=0.2,random_state=42)


# In[87]:


X_train.shape,X_test.shape,Y_train.shape,Y_test.shape


# ## Normalization

# In[88]:


from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)


# In[90]:


get_ipython().system('pip install mlxtend')


# In[91]:


from sklearn.model_selection import RandomizedSearchCV
from sklearn.linear_model import LinearRegression,Lasso,ElasticNet,Ridge
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from catboost import CatBoostRegressor
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
from lightgbm import LGBMRegressor
from xgboost import XGBRegressor
from mlxtend.regressor import StackingCVRegressor
from sklearn.metrics import mean_squared_log_error


# In[92]:


lr = LinearRegression()
lr.fit(X_train,Y_train)
pred = np.expm1(lr.predict(X_test))
s1 = np.sqrt(mean_squared_log_error(Y_test,pred))
s1


# In[93]:


ls = Lasso(alpha=1)
ls.fit(X_train,Y_train)
pred_ls = np.expm1(ls.predict(X_test))
s2 = np.sqrt(mean_squared_log_error(Y_test,pred_ls))
s2


# In[94]:


rd = Ridge(alpha=1)
rd.fit(X_train,Y_train)
pred_rd = np.expm1(rd.predict(X_test))
s3 = np.sqrt(mean_squared_log_error(Y_test,pred_rd))
s3


# In[95]:


en = ElasticNet(alpha=1)
en.fit(X_train,Y_train)
pred_en = np.expm1(en.predict(X_test))
s4 = np.sqrt(mean_squared_log_error(Y_test,pred_en))
s4


# In[96]:


dt = DecisionTreeRegressor()
dt.fit(X_train,Y_train)
pred_dt = np.expm1(dt.predict(X_test))
s5 = np.sqrt(mean_squared_log_error(Y_test,pred_dt))
s5


# In[97]:


cat = CatBoostRegressor(iterations=500,loss_function='MAE',eval_metric='RMSE',task_type='GPU')
cat.fit(X_train,Y_train,verbose=True)
pred_cat = np.expm1(cat.predict(X_test))
s6 = np.sqrt(mean_squared_log_error(Y_test,pred_cat))
s6


# In[98]:


rf = RandomForestRegressor()
rf.fit(X_train,Y_train)
pred_rf = np.expm1(rf.predict(X_test))
s6 = np.sqrt(mean_squared_log_error(Y_test,pred_rf))
s6


# In[99]:


params = {'n_estimators': 1000,
          'max_depth': 4,
          'min_samples_split': 5,
          'learning_rate': 0.01,
          'loss': 'ls'}
gd =GradientBoostingRegressor(**params)
gd.fit(X_train,Y_train)
pred_gd = np.expm1(gd.predict(X_test))
s7 = np.sqrt(mean_squared_log_error(Y_test,pred_gd))
s7


# In[100]:


test_score = np.zeros((params['n_estimators'],), dtype=np.float64)
for i, y_pred in enumerate(gd.staged_predict(X_test)):
    test_score[i] = gd.loss_(Y_test, y_pred)

fig = plt.figure(figsize=(6, 6))
plt.subplot(1, 1, 1)
plt.title('Deviance')
plt.plot(np.arange(params['n_estimators']) + 1, gd.train_score_, 'b-',
         label='Training Set Deviance')
plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
         label='Test Set Deviance')
plt.legend(loc='upper right')
plt.xlabel('Boosting Iterations')
plt.ylabel('Deviance')
fig.tight_layout()
plt.show()


# In[101]:


xg = XGBRegressor(n_estimators = 3000,learning_rate=0.01)
xg.fit(X_train,Y_train)
pred_xg = np.expm1(xg.predict(X_test))
s8 = np.sqrt(mean_squared_log_error(Y_test,pred_xg))
s8


# In[102]:


lgb = LGBMRegressor()
lgb.fit(X_train,Y_train,eval_set = (X_test,Y_test),early_stopping_rounds=1000)
pred_lgb = np.expm1(lgb.predict(X_test))
s9 = np.sqrt(mean_squared_log_error(Y_test,pred_lgb))
s9


# In[103]:


# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 100, stop = 1200, num = 12)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(5, 30, num = 6)]
# max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10, 15, 100]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 5, 10]
random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf}
rf_random = RandomizedSearchCV(estimator=rf,param_distributions=random_grid,
                               scoring='neg_mean_squared_error',
                              n_iter=10,cv=5,verbose=2,random_state=42,n_jobs=1)
rf_random.fit(X_train,Y_train)


# In[104]:


rf_random.best_params_


# In[105]:


predictions = np.expm1(rf_random.predict(X_test))
s10 = np.sqrt(mean_squared_log_error(Y_test,predictions))
s10


# In[106]:


test.instock_date = pd.to_datetime(test['instock_date'])
test['instock_date_year'] = test['instock_date'].dt.year
test['instock_date_month'] = test['instock_date'].dt.month
test['instock_date_week'] = test['instock_date'].dt.week
test['instock_date_day'] = test['instock_date'].dt.day
test['instock_date_hour'] = test['instock_date'].dt.hour
test['instock_date_minute'] = test['instock_date'].dt.minute
test['instock_date_dayofweek'] = test['instock_date'].dt.dayofweek


# In[107]:


test.drop(['Product_id','Customer_name','Stall_no','instock_date'],axis=1,inplace=True)


# In[108]:


test = pd.get_dummies(test,columns=['Loyalty_customer','Product_Category','Grade','Discount_avail'],drop_first=True)


# In[109]:


test.isnull().sum()


# In[110]:


from sklearn.impute import SimpleImputer
imputer = SimpleImputer(missing_values =np.NaN,strategy='median')
imputer.fit(test)
x = imputer.transform(test)


# In[111]:


test_new = pd.DataFrame(x,columns=test.columns)


# In[112]:


test_new.isnull().sum()


# In[113]:


test_new.head()


# In[114]:


test = sc.transform(test_new)


# In[ ]:


## To be Contd.

